(function(){
  var app = angular.module('appBacenStaWS', []);

  app.controller('bswAppController', function($scope, bswAppUtilsService) {

    this.tab = 1;
    $scope.acessoId = 0;

    this.setTabService = function(setTab, setServiceID) {
      this.tab = setTab;
      this.setServiceID(setServiceID);
    };

    this.isSelected = function(checkTab){
      return this.tab === checkTab;
    };

    this.setServiceID= function(setServiceID) {
      $scope.acessoId = setServiceID;
    };

    this.callBackSetupJson = function(d){
      $scope.setup = d.data.setup;
    };

    this.callBackAcessosJson = function(d){
      $scope.acessosCadastrados = d.data;
    };

    bswAppUtilsService.getJsonData('data/setup.json', this.callBackSetupJson);
    bswAppUtilsService.getJsonData('data/acessos.json', this.callBackAcessosJson);

  });

  app.service('bswAppUtilsService', ['$http', function($http) {

    this.encodeBase64 = function( str ) {
      return btoa( str );
    };

    this.decodeBase64 = function( str ) {
      return atob( str );
    };

    this.getJsonData = function( fileName, f ) {
      var json = this;

      $http.get( fileName ).success(function(data){
        json.data = data;
      }).error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        // status is the HTTP status
        // headers is the header getter function
        // config is the object that was used to create the HTTP request
        console.log("Ocorreu erro ao obter arquivo ("+fileName+") json!");
      }).then(function(d){
        f(d)
      });

      return json.data;
    };

    this.getBacenData = function( authorization, URL, callBack ) {
      var bacenResponse = this;

      $http.defaults.headers.common.Authorization = authorization;

      $http.get( URL ).success(function(data){
        bacenResponse.data = data;
      }).error(function(data, status, headers, config) {
        // called asynchronously if an error occurs
        // or server returns response with an error status.
        // data contains the response
        // status is the HTTP status
        // headers is the header getter function
        // config is the object that was used to create the HTTP request
        callBack(data, status, headers, config);
        console.log("Ocorreu erro ao obter dados do Bacen! [data: "+
                  data+" st: "+status+" H: "+headers+" cfg: "+config+"]");
      }).then(function(d){
        callBack(d);
      });

    };

  } ]);

  app.directive('bswEditar', function(){
    return {
      restrict: 'E',
      templateUrl: 'views/bsw-editar.html'
    };
  });

  app.controller('bswEditarController', function($scope, bswAppUtilsService){

    this.setup = $scope.setup;
    this.editarAcesso = $scope.acessosCadastrados[$scope.acessoId-1];

    $scope.passwordType = 'password';

    this.btnClick = function(btnClick) {
      if (btnClick == 'cancel') {
        this.setTabService(1); // cancela e lista acessos

      } else if (true) {

      }

    };

    this.callbackVerificarAcesso = function(httpData,status,headers,config) {
      var verificado = false;
      var x2js = new X2JS();
      var reqStatus = httpData.status || status;

      if(reqStatus == 200) { // nao ocorreu erro
        $scope.statusMessage = "Verificação com sucesso!";
        //window.open('data:text/xml,' + httpData.data);
        //$scope.xml2json = x2js.xml_str2json(httpData.data);
      } else { // ocorreu erro
        $scope.statusMessage = "Erro de conexão ("+reqStatus+")!";
      }

      //this.editarAcesso.verified = verificado;
    };

    this.verificarAcesso = function(editarAcesso) {
      $scope.statusMessage = "";
      var userURL = editarAcesso.bcbOrg+editarAcesso.bcbDep+"."+editarAcesso.bcbOper;
      var passURL = editarAcesso.bcbPass;
      var urlTestConn = $scope.setup.ambiente[editarAcesso.ambiente] +
                        $scope.setup.urlTestConn.resumido;
      var authorization = 'Basic '+bswAppUtilsService.encodeBase64(userURL+":"+passURL);

      $scope.verificaURL = authorization+ " - url: "+urlTestConn;
      bswAppUtilsService.getBacenData(authorization,urlTestConn,this.callbackVerificarAcesso);

    };

    this.salvarAcesso = function(editarAcesso){
      $scope.statusMessage = "";

      // atualiza valores conforme editação realizada
      editarAcesso.urlTestConn = $scope.setup.ambiente[editarAcesso.ambiente];
      $scope.acessosCadastrados[$scope.acessoId-1] = editarAcesso;

      $scope.statusMessage = "Dados salvos em memória!";

      // persistir dados alterados

      //$scope.statusMessage = "Dados salvos em arquivo!";

      $scope.testesalvar = $scope.acessosCadastrados;

    };

    this.togglePasswordFieldType = function() {
      if( $scope.passwordType == 'password' ) {
        $scope.passwordType = 'text';
      } else{
        $scope.passwordType = 'password';
      };
    };

  });

  app.directive('bswAcessosCadastrados', function(){
    return {
      restrict: 'E',
      templateUrl: 'views/bsw-listar.html'
    };
  });

  app.directive('bswListaDetalheAcessos', function(){
    return {
      restrict: 'E',
      templateUrl: 'views/bsw-lista-detalhe-acessos.html'
    };
  });

  app.directive('bswIncluir', function(){
    return {
      restrict: 'E',
      templateUrl: 'views/bsw-incluir.html'
    };
  });

  app.directive('bswSobre', function(){

    // Wait for device API libraries to load
    //
    document.addEventListener("deviceready", onDeviceReady, false);

    // device APIs are available
    //
    function onDeviceReady() {
      var appVersion = document.getElementById('appVersion');
      var element = document.getElementById('deviceProperties');
      element.innerHTML = 'Device Name: '     + device.name     + '<br />' +
                          'Device Cordova: '  + device.cordova  + '<br />' +
                          'Device Platform: ' + device.platform + '<br />' +
                          'Device UUID: '     + device.uuid     + '<br />' +
                          'Device Model: '    + device.model    + '<br />' +
                          'Device Version: '  + device.version  + '<br />';

      // app version (from config.xml)
      cordova.getAppVersion(function (version) {
        appVersion.innerHTML = version;
      });

    }

    return {
      restrict: 'E',
      templateUrl: 'views/bsw-sobre.html'
    };
  });

  app.controller('bswAcessosController', function($scope) {
    var acessos = this;

    setTimeout(function () {
        $scope.$apply(function () {
            acessos.lista = $scope.acessosCadastrados;
        });
    });

  });

  app.directive('bswConfigurar', function(){
    return {
      restrict: 'E',
      templateUrl: 'views/bsw-configurar.html'
    };
  });

  app.controller('bswSetupController', function($scope, bswAppUtilsService) {
    var editSetup = 0;
    var setup = this;
    setup.data = $scope.setup;

  } );

})();
